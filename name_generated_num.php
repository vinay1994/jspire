<?php
/*
Function Name:name_random_num
	name_random_num is a function which accepts name as a parameter.
Function Action:
	Generates a random number between 6 and 15 and appends it with the name given.
Result:
	Returns a string in the format “Name GeneratedNumber”

*/
function name_random_num(name){
    return name." ".rand(6,15);
}

?>
